import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * This is a white blood cell. This kind of cell has the job to catch 
 * bacteria and remove them from the blood.
 * 
 * @author jpunlay@email.uscb.edu
 * @version 11-04-15
 */
public class WhiteCell extends Actor
{
    /*
     * Instance variables
     */
    
    
    
    /**
     * Act: move up and down when cursor keys are pressed.
     */
    public void act() 
    {
        checkKeyPress();
        checkCollision();
        
    }//end act method
    
    /**
     * Check whether a keyboard key has been pressed and react if it has.
     */
    private void checkKeyPress()
    {
        if (Greenfoot.isKeyDown("up")) 
        {
            setLocation(getX(), getY()-8);//5.37 speed changed to 8
        }//end if
        
        if (Greenfoot.isKeyDown("down")) 
        {
            setLocation(getX(), getY()+8);//5.37 speed changed to 8
        }//end if
        
        if (Greenfoot.isKeyDown("left")) 
        {
            setLocation(getX()-4, getY());//5.29 left movement added
        }//end if
        
        if (Greenfoot.isKeyDown("right")) 
        {
            setLocation(getX()+4, getY());//5.29 right movement added
        }//end if
        
    }
    
    /**
     * Will check for collision between WhiteCells and other objects
     */
    private void checkCollision()
    {
        if (isTouching(Bacteria.class))
        {
            removeTouching(Bacteria.class); //5.14 Will remove the bacteria
            Greenfoot.playSound("bite.wav"); //5.15 Will play sound after Bacteria is removed
            Bloodstream bloodstream =(Bloodstream)getWorld(); //5.42 casting the method addScore-unclear
            bloodstream.addScore(20); //5.44 value changed to expected parameter value type
        }//end if
        
        if (isTouching(Virus.class))
        {
            Greenfoot.playSound("no.wav");// 5.17 sound will be played when Virus is touched
            removeTouching(Virus.class); //5.48 Virus is removed when in contact
            Bloodstream bloodstream =(Bloodstream)getWorld();
            bloodstream.addScore(-100); //5.48 virus makes player lose 100 points
        }
    }//end checkCollision method
    
}
