import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The Lining are objects at the edge of the vein.
 * 
 * @author jpunlay@email.uscb.edu   
 * @version 11-04-15
 */
public class Lining extends Actor
{
    /**
     * Act- move Lining across the x-axis and remove once it reaches the edge
     * 
     */
    public void act() 
    {
        setLocation( getX()-1, getY()); // 5.5 MAke lining move left one cell per act
        
        if (getX() == 0)
        {
            getWorld().removeObject(this); //5.6 remove lining once it reaches the edge of the world
            
        }//end if
    }//end of method act    
}
