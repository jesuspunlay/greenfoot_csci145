import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Bacteria fload along in the bloodstream. They are bad. Best to destroy
 * them if you can.
 * 
 * @author Michael Kölling
 * @version 0.1
 */
public class Bacteria extends Actor
{
    /*
     * Instance variables
     */
    
    private int speed; // 5.18 instance variable speed created
    
    /**
     * Constructor.
     */
    public Bacteria()
    {
        speed = Greenfoot.getRandomNumber(5)+1; // 5.19 random value asigned to the variable speed
                                                //5.37 Bacteria's speed changed to a random number between 1 and 5
    }//end constructor

    /**
     * Float along the bloodstream, slowly rotating.
     */
    public void act() 
    {
        setLocation(getX()-speed, getY());
        turn(1);
        if (getX() == 0) 
        {
            Bloodstream bloodstream =(Bloodstream)getWorld(); //5.45 casting the method addScore-unclear
            bloodstream.addScore(-15); //5.45 bacteria will decrease 15 points
            bloodstream.removeObject(this);
        }//end if
        
    }//end act method
   
}//end class Bacteria
