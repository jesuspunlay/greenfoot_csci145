import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * The bloodstream is the setting for our White Blood Cell scenario. 
 * It's a place where blood cells, bacteria and viruses float around.
 * 
 * @author jpunlay@email.uscb.edu
 * @version 11-04-15
 */
public class Bloodstream extends World
{
    /*
     * Instance variables
     */
    private int score; // 5.38 score moved to bloodstream
    private int time; //5.51 instant variable for a timer
    
    
    /**
     * Constructor: Set up the staring objects.
     */
    public Bloodstream()
    {    
        super(780, 360, 1); 

        prepare();
        
        setPaintOrder(Border.class); // 5.28 border positioned over any other object
        
        score = 0; //5.39 score initialized to 0
        
        showScore();//5. 47 Score will display at the start
        
        time = 2000; //5.52 timer initialized
        
        showTime();//5.54 counter will display at the start
    }//end constructor
    
    /**
     * private method for the timer
     */
    private void showTime()
    {
        showText ("Time left:"+ time, 720, 25); //5.54 calling showText again to display the timer
        
    }//end method showTime
    
    /**
     * private method counTime, to decrease the timer
     * 
     */
    private void countTime()
    {
        time = time-1; //5.54 timer is decreased by 1 every act
        showTime(); 
        
        if (time ==0 )
        {
            Greenfoot.stop();//5.55 game will stop when timer reaches 0
            showEndMessage(); //5.56 game will call method shoEndMessage when it reaches 0
        }//end if
    }//end method countTime
    
    /**
     * Create new floating objects at irregular intervals.
     * create new Lining every 100 cycles.
     */
    public void act()
    {
        if (Greenfoot.getRandomNumber(100) < 3)
        {
            addObject(new Bacteria(), 779, Greenfoot.getRandomNumber(360));
        }//end if
        
        if (Greenfoot.getRandomNumber(100) <1)
        {
            addObject(new Lining(), 779, 1); // 5.8 make Lining appear on the right every 100 cycles
            addObject(new Lining(), 779, 359);// 5.8 make Lining appear on the right every 100 cycles
        }//end if
        
        if (Greenfoot.getRandomNumber(100) <1)
        {
            addObject(new Virus(), 779, Greenfoot.getRandomNumber(360));// 5.10 Virus will appear every 100 cycles
        }//end if
        
        if (Greenfoot.getRandomNumber(100) < 6)
        {
            addObject(new RedCell(), 779, Greenfoot.getRandomNumber(360)); //5.23 RedCells will be randomly placed
                                                                           //in the world at a 6% chance
        }//end if
        
        countTime();
    }
    
    /**
     * Prepare the world for the start of the program. In this case: Create
     * a white blood cell and the lining at the edge of the blood stream.
     */
    private void prepare()
    {
        WhiteCell whitecell = new WhiteCell();
        addObject(whitecell, 95, 179);// 5.27 value for the x-axis of the
                                      //white cell changed to a larger value
        Lining lining = new Lining();
        addObject(lining, 126, 1);
        Lining lining2 = new Lining();
        addObject(lining2, 342, 5);
        Lining lining3 = new Lining();
        addObject(lining3, 589, 2);
        Lining lining4 = new Lining();
        addObject(lining4, 695, 5);
        Lining lining5 = new Lining();
        addObject(lining5, 114, 359);
        Lining lining6 = new Lining();
        Lining lining7 = new Lining();
        addObject(lining7, 295, 353);
        Lining lining8 = new Lining();
        Lining lining9 = new Lining();
        Lining lining10 = new Lining();
        addObject(lining10, 480, 358);
        Lining lining11 = new Lining();
        addObject(lining11, 596, 359);
        Lining lining12 = new Lining();
        addObject(lining12, 740, 354);
        
        Border border = new Border(); //5.26 borders have been created and added to the scenario
        addObject(border, 0, 180);
        Border border2 = new Border();
        addObject(border2, 770, 180);
    }//end prepare
    
    /**
     * score counting moved to Bloodstream, getworld() now omitted 
     */
    public void addScore(int points)
    {
        score = score + points;//5.43 score will increse by the int amount entered
        showScore(); //5.46 calling to the method showScore
        
        if (score < 0)
        {
            Greenfoot.playSound ("game-over.wav"); //5.50 Game will play sound if socre is below zero
            Greenfoot.stop(); //5.50 Game will stop once it reaches a score of less than 0
        }//end if
    }//end method addScore
    
    /**
     * 5.46 private method showScore() created
     */
    private void showScore()
    {
        showText("Score:"+ score, 80, 25);// 5.46 showText moved to showScore
    }//end method showScore

    private void showEndMessage()
    {
        showText ("Time is up - you rock!", 385, 150);//5.56 end of game message
        showText ("Your Score is: " + score + " points" , 385, 180); //5.56 end of game score
    }//end method showEndMessage
    
}//end class
