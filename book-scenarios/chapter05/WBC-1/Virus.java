import greenfoot.*;

/**
 * 
 * @author jpunlay@email.uscb.edu 
 * @version 11-04-15
 */
public class Virus extends Actor
{
    /**
     * Act - Virus will move across the x-axis and rotate counter-clockwise
     * 
     */
    public void act() 
    {
        setLocation(getX()-8, getY()); //5.11 Virus will move across x-axis
                                       //5.37 speed changed to 8
        turn(-1); // 5.11 Virus will rotate counter-clockwise
        
    }//end act method
}//end class Virus
