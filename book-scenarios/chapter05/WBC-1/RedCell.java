import greenfoot.*;

/**
 * Red cells float along the bloodstream
 * 
 * @author jpunlay@email.uscb.edu
 * @version 11-04-15
 */
public class RedCell extends Actor
{
    /*
     * Instance variables
     */
    
    private int speed; // 5.22 instance variable speed created
    
    /**
     * Constructor. Nothing to do so far.
     */
    public RedCell()
    {
        speed = Greenfoot.getRandomNumber(2)+1; // 5.22 random value asigned to the variable speed
        setRotation(Greenfoot.getRandomNumber(360));//5.24 random rotation set for RedCells
    }//end constructor

    /**
     * Float along the bloodstream, slowly rotating.
     */
    public void act() 
    {
        setLocation(getX()-speed, getY());
        turn(1);
        if (getX() == 0) 
        {
            getWorld().removeObject(this);
        }//end if
        
    }//end act method
}//end class RedCell
