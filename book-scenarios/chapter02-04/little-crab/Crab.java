import greenfoot.*;

/**
 * This class defines a crab. Crabs live on the beach.
 */
public class Crab extends Actor
{
    public void act()
    {       
        if (isAtEdge())
        {
            turn(17);
        }//end if
        
        if (Greenfoot.getRandomNumber(100) < 10) //at random, 10 percent of the time (on average)
        {
            turn ( Greenfoot.getRandomNumber(91)-45 );
        }//end if
        
        move(2); //crab will move 5 pixels per cycle
        
        if ( isTouching(Worm.class))
        {
            removeTouching (Worm.class);
        }//end if
        
        if ( isTouching(pizza.class))
        {
            removeTouching (pizza.class);
        }//end if
    } // end method act
}//end class crab

