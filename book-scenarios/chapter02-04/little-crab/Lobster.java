import greenfoot.*;

/**
 * Write a description of class Lobster here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Lobster extends Actor
{
    /**
     * Act - do whatever the Lobster wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
      
        if (isAtEdge())
        {
            turn(17);
        }//end if
        
        if (Greenfoot.getRandomNumber(100) < 10) //at random, 10 percent of the time (on average)
        {
            turn ( Greenfoot.getRandomNumber(91)-45 );
        }//end if
        
        move(2); //crab will move 5 pixels per cycle
        lookForWorm();
    }
    
    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing.
     */
    
    public void lookForWorm()   
    {
        if ( isTouching(Crab.class))
        {
            removeTouching(Crab.class);
        }//end if
    }//end method lookFormWorm 
}//end class lobster
