import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * @author jpunlay@email.uscb.edu
 * @version csci145_hw2_ex1
 * 
 * In this version, the crab behaves as before, but we add animation of the 
 * image.
 * 
 * */

public class Crab extends Actor
{
    /**
     * instance variables
     */ 
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int frameCount;
    private int currentFlipAngle;

    /**
     * Create a crab and initialize its two images.
     */
    public Crab()
    {
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");
        setImage(image1);
        wormsEaten = 0;
        frameCount=0;//initializing frameCount
        currentFlipAngle=0;
    }//end crab constructor

    /** 
     * Act - do whatever the crab wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        lookForWorm();
        //switchImage();
        frameCount++; //frameCount will add 1 to the current frame
        checkFlipAround();

    }//end method act

    /**
     * Alternate the crab's image between image1 and image2.
     */
    public void switchImage()
    {
        if(frameCount==4)
        {
            if (getImage() == image1) 
            {
                setImage(image2);
            }//if
            else
            {
                setImage(image1);
            }//end if/else

            //reseting frameCount back to 0 to repeat process
            frameCount=0;
        }//end outer if
    }//end method switchImage

    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    {
          if (Greenfoot.isKeyDown("up"))
        {
            move(5);//move    
            if (getImage() == image1) 
            {
                setImage(image2);
            }//if
            else
            {
                setImage(image1);
            }//end if/else
        }//end if
        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
            if (getImage() == image1) 
            {
                setImage(image2);
            }//if
            else
            {
                setImage(image1);
            }//end if/else
        }//end if
        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);
            if (getImage() == image1) 
            {
                setImage(image2);
            }//if
            else
            {
                setImage(image1);
            }//end if/else
        }//end if
       
    }//end method checkKeypress

    /**
     * The crab will quickly flip 45 degrees when pressing the down key,
     * While holding the key it will keep turning 45 until it has turned past 180 degrees
     */
    public void checkFlipAround()
    {
        if (Greenfoot.isKeyDown("down"))
        {
            if (getImage() == image1) 
            {
                setImage(image2);
            }//if
            else
            {
                setImage(image1);
            }//end if/else
            if (currentFlipAngle<180)
            {
                turn(45);
                currentFlipAngle = currentFlipAngle +45;
            }//end inner if

        }//end if
        else 
        {
            currentFlipAngle= 0;
        }//end else
    }//end method checkFlipAround

    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing. If we have
     * eaten eight worms, we win.
     */
    public void lookForWorm()
    {
        if ( isTouching(Worm.class) ) 
        {
            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");

            wormsEaten = wormsEaten + 1;

            if (wormsEaten == 8) 
            {
                Greenfoot.playSound("fanfare.wav");
                Greenfoot.stop();
            }//end if
        }//end if
    }//end method lookForWorm
}