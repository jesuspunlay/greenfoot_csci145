import greenfoot.*;  // (Actor, World, Greenfoot, GreenfootImage)

public class CrabWorld extends World
{
    /**
     * Create the crab world (the beach). Our world has a size 
     * of 560x560 cells, where every cell is just 1 pixel.
     */
    public CrabWorld() 
    {
        // define the size (in cells) and resolution (in pixels per cell) of the
        // CrabWorld
        super(560, 560, 1);

        /*
        // Instantiate a new crab object
        // and assign it to a new variable called myCRab
        Crab myCrab = new Crab();

        // Place the newly crated crab obaject into the world
        // at x=250, y=200
        addObject( myCrab, 250, 200 );

        //add three lobster objects to the crab world
        // at arbitrary locations
        Lobster lobster1 = new Lobster();
        Lobster lobster2 = new Lobster();
        Lobster lobster3 = new Lobster();

        addObject( lobster1, 150, 100);
        addObject( lobster2, 500, 500);
        addObject( lobster3, 50, 50);

        //add two worms to the world
        // at arbitrary locations

        Worm worm1 = new Worm();
        Worm worm2 = new Worm();

        addObject (worm1, 500, 100);
        addObject (worm2, 80, 120);
         */

        prepare();
    }// end CrabWorld contrstuctor

    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    private void prepare()
    {
        Crab crab = new Crab();
        addObject(crab, 280, 244);
        Crab crab2 = new Crab();
        addObject(crab2, 186, 124);
        removeObject(crab2);
        Lobster lobster = new Lobster();
        addObject(lobster, 134, 123);
        Lobster lobster2 = new Lobster();
        addObject(lobster2, 426, 122);
        Lobster lobster3 = new Lobster();
        addObject(lobster3, 177, 444);
        Worm worm = new Worm();
        addObject(worm, 479, 339);
        Worm worm2 = new Worm();
        addObject(worm2, 290, 146);
        Worm worm3 = new Worm();
        addObject(worm3, 460, 41);
        Worm worm4 = new Worm();
        addObject(worm4, 520, 41);
        Worm worm5 = new Worm();
        addObject(worm5, 167, 288);
        Worm worm6 = new Worm();
        addObject(worm6, 316, 393);
        Worm worm7 = new Worm();
        addObject(worm7, 494, 494);
        Worm worm8 = new Worm();
        addObject(worm8, 36, 453);
        Worm worm9 = new Worm();
        addObject(worm9, 157, 513);
        Worm worm10 = new Worm();
        addObject(worm10, 61, 58);
    }
}// end class CrabWorld
