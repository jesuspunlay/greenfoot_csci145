import greenfoot.*;  // (World, Actor, GreenfootImage, and Greenfoot)

/**
 * This class defines a crab. Crabs live on the beach. They like sand worms 
 * (very yummy, especially the green ones).
 * 
 * Version: 4
 * 
 * The crab is keyboard controlled and eats worms. In this version, we have added
 * a sound when the crab eats a worm.
 */

public class Crab extends Actor
{
    /*
     * Instance variables (also called fields)
     */ 
    private GreenfootImage image1;
    private GreenfootImage image2;
    private int wormsEaten;
    private int frameCount;

    /*
     * constructor  
     */
    public Crab()
    {
        //create (instantiate) the GreenfotImage objects
        //and store them in our instace variables
        image1 = new GreenfootImage("crab.png");
        image2 = new GreenfootImage("crab2.png");

        //set the current image of the CRab object to image1
        setImage (image1);

        //initialize worms eaten to zero
        //(even though it already has a default value of zero)
        wormsEaten= 0;
         
        //initialize framCount (even though it already has a default value to 0)
        frameCount = 0;

    }//end CRab constructor

    /** Act - do whatever the crab wants to do. This method is called whenever
     *  the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act()
    {
        checkKeypress();
        move(5);
        lookForWorm();
        switchImage();
        frameCount++;
    }//end method act

    /**
     * Alternate the crab's image between image1 and image2
     */

    public void switchImage()
    {
        if(frameCount==5)
        {

            if ( getImage()== image1 )
            {
                setImage (image2);
            }//else
            else //otherwise assume that current image is image2
            {
                setImage( image1 );
            }//en if/else
            
            //reset framCount back to zero
            frameCount=0;
        }//end outer if
    }//end method switchImage

    /**
     * Check whether a control key on the keyboard has been pressed.
     * If it has, react accordingly.
     */
    public void checkKeypress()
    {
        if (Greenfoot.isKeyDown("left")) 
        {
            turn(-4);
        }//end if

        if (Greenfoot.isKeyDown("right")) 
        {
            turn(4);
        }//end if
    }//end method checkKeyPress

    /**
     * Check whether we have stumbled upon a worm.
     * If we have, eat it. If not, do nothing.
     */
    public void lookForWorm()
    {
        if ( isTouching(Worm.class) ) 
        {
            removeTouching(Worm.class);
            Greenfoot.playSound("slurp.wav");

            //increment the number of worms eaten by 1
            wormsEaten = wormsEaten + 1;//updated wormsEaten value

            //could also replace the above statement with either 
            //of the following:

            //wormsEaten++;
            //wormsEaten += 1;

            //end the game when the crab has eaten a pre-specified number of worms
            if (wormsEaten == 8 )
            {
                Greenfoot.playSound("fanfare.wav");
                System.out.print.ln("You win!");
                Greenfoot.stop();
                
                
            }//end inner if

        }//end outer if
    }//end method lookForWorm
    /**
    public int getX()
    {
        return x;
    }
    *
    */
    }//end class crab