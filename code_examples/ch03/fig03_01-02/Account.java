// Fig. 3.1: Account.java
// Account class that contains an name instance variable 
// and methods to set and get its value.

public class Account
{
   /*Fields*/
   private String name; // PRIVATE instance variable

   /*CONSTRUCTOR(S)*/
   /*
   * NO EXPLICITY-DEFINED CONSTRUCTOR , SO JAVA WILL
   * PREPARE A DEFAULT CONSTRUCTOR FOR US. THIS DEFAULT CONSTRUCTO HAS NO ARGUMENTS
   * (NO PARAMETERS)
   */

   /*
   * Methods
   * when you define a field ( an instance variable),
   * it is often good idea to encapsulate each field 
   * by defining public set and get methods, like those below:
   */
 
   // PUBLIC method to set the name in the object              
   public void setName(String name)      
   {  
   /*
   NOTE THAT THERE ARE TWO VARIBLAES WITH THE SAME NAME!
   WHICH ONE IS A LOCAL VARIABLE, HAVING METHOD-WIDE SCOPE?
   WHICH ONE IS THE INSTANCE VARIABL, HAVING CLASS-WIDE SCOPE?
   WHERE WAS THE LOCAL VAIABLE ACTUALLY DECLARED?
   THIS.NAME=NAME; //STORE THE NAME
   */                                           
      this.name = name; // store the name
   } // END METHOD setName        

   // PUBLIC method to retrieve the name from the object
   public String getName()        
   {         
      //RETURNING THE VALUE OF THE INSTANCE VARIABLE NAME
      //(NOTE THAT NO LOCAL VARIABLE WAS DECLARED INSIDE THIS METHOD)  
      //RETURN HAS TO MATCH THE RETURN TYPE FOR THE METHOD(STRING, IN THIS CASE)                         
      return name; // return value of name to caller        
   } //end method setName       
} // end class Account


/**************************************************************************
 * (C) Copyright 1992-2014 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
